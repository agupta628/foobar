def answer(l):
    def version_sort(index_count, arr):
        if len(arr) < 2:
            return arr

        versions = set()
        sort_queue = []
        answer_array = []
        
        for element in arr:
            element_split = element.split('.')
            
            if len(element_split) <= index_count:
                answer_array.append(element)
            else:
                versions.add(element_split[index_count])
                sort_queue.append(element)
            
        sort_queue = sorted(sort_queue, key=lambda x: x.split('.')[index_count])
        resultant_array = []

        for version in sorted(list(versions), key=lambda x: int(x)):
            current_version = list(filter(lambda x: x.split('.')[index_count] == version, sort_queue))

            for e in answer_array:
                if e.split('.')[index_count - 1] == current_version[0].split('.')[index_count - 1] and e not in resultant_array:
                    resultant_array.append(e)
            
            resultant_array += version_sort(index_count + 1, current_version)
        
        return resultant_array

    return version_sort(0, l)

#print(answer(["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"]))
print(answer(["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"]))
#print(answer(["1.0", "1.0.12", "1.0.2"]))
