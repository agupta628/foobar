def answer(n, b):
    def base10_to_customBase(num, base):
        converted_string, modstring = "", ""
        currentnum = num
        
        if not num:
            return '0'
        
        while currentnum:
            mod = currentnum % base
            currentnum = currentnum // base
            converted_string = chr(48 + mod + 7*(mod > 10)) + converted_string

        return converted_string

    previously_visited = []
    k = len(n)

    while True:
    #while n not in previously_visited:
        n_numbers_array = list(n)

        x = sorted(n_numbers_array, key=lambda x: int(x), reverse=True)
        y = sorted(n_numbers_array, key=lambda x: int(x))

        x = ''.join(x)
        y = ''.join(y)

        x_base_10 = int(x, b)
        y_base_10 = int(y, b)

        z_base_10 = x_base_10 - y_base_10
        z = base10_to_customBase(z_base_10, b)
        z = z.zfill(k)

        n = z

        if z in previously_visited:
            break

        previously_visited.append(z)

    index_of_prev_z = previously_visited.index(n) 
    distance = len(previously_visited) - index_of_prev_z  

    return distance

answer("210022", 3)
answer("1211", 10)
