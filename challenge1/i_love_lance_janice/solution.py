def answer(s):
    alphabet_lower = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    alphabet_upper = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    resultant_string = ''

    for c in s:
        if not c.isalpha() or c in alphabet_upper:
            resultant_string += c
            continue

        numerical_rep = alphabet_lower.index(c)
        new_letter_index = 25 - numerical_rep

        resultant_string += alphabet_lower[new_letter_index]

    return resultant_string
