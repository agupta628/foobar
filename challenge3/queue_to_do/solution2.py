def answer(start, length):
    def xor(a):
        xor_list = [a, 1, a + 1, 0]
        return xor_list[a % 4]
    
    def row_xor(row):
        # print(start)
        # print(length)
        # print(row)

        start_of_row = start + length * row
        end_of_row = start_of_row + length - (row + 1)

        # print("Start of Row")
        # print(start_of_row)
        # print("End of Row")
        # print(end_of_row)
        # print("xor")
        # print(xor(start_of_row - 1) ^ xor(end_of_row)) 
        # print("\n")
        
        if row == length - 1:
            return start_of_row

        return xor(start_of_row - 1) ^ xor(end_of_row)

    checksum = row_xor(0)
    print(row_xor(0))
    for i in range(1, length):
        print(i)
        print(row_xor(i))
        print("\n")

        checksum = checksum ^ row_xor(i)

    return checksum

print(answer(0, 300))
