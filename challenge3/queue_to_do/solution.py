def answer(start, length):
    row = length
    col = length
    
    current_number = start
    numbers_to_xor = []
    for i in range(row):
        for j in range(col):
            numbers_to_xor.append(current_number)
            current_number += 1

        current_number += length - col
        col -= 1

    answer = numbers_to_xor[0]
    for num in numbers_to_xor[1:]:
        answer = answer ^ num
    
    return answer

for i in range(1, 300):
    print("start: 0 length: " + str(i) + " checksum: " + str(answer(0, i)))

# for i in range(1, 30):
#     print("start: " + str(i) + " length: 4 checksum: " + str(answer(i, 4)))
