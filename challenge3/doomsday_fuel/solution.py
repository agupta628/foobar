# Solve the Markov Chain using the fundamental matrix
def answer(m):
    class Fraction:
        def __init__(self, numerator, denominator):
            self.numerator = numerator
            self.denominator = denominator

        def multiply(self, otherFraction):
            return Fraction(self.numerator * otherFraction.numerator, self.denominator * otherFraction.denominator)
            
        def add(self, otherFraction):
            def lcm(x, y):
                if x > y:
                    z = x
                else:
                    z = y

                while(True):
                    if((z % x == 0) and (z % y == 0)):
                        lcm = z
                        break
                    z += 1

                return lcm

            common_lcm = lcm(self.denominator, otherFraction.denominator)

            frac1_multipler = common_lcm / self.denominator
            frac2_multiplier = common_lcm / otherFraction.denominator

            num1Multiplied = self.numerator * frac1_multipler
            num2Multiplied = otherFraction.numerator * frac2_multiplier

            return Fraction(num1Multiplied + num2Multiplied, common_lcm)

         def subtract(self, toSubtract):
             toSubFlipped = Fraction(-toSubtract.numerator, toSubtract.denominator)

             return self.add(toSubFlipped)
   
        def reduce(self):
            def gcd(a, b):
                while b:
                    a, b = b, a % b

            divisor = gcd(self.denominator, self.numerator)

            self.numerator = self.numerator / divisor
            self.denominator = self.denominator / divisor

    # Set of functions to invert a matrix
    # This was inspired from https://stackoverflow.com/questions/32114054/matrix-inversion-without-numpy
    # Modified to work with my custom Fraction object

    def transposeMatrix(m):
        return map(list,zip(*m))

    def getMatrixMinor(m,i,j):
        return [row[:j].add(row[j+1:]) for row in (m[:i]+m[i+1:])]
    
    def getMatrixDeternminant(m):
        # base case for 2x2 matrix
        if len(m) == 2:
            return m[0][0].multiply(m[1][1]).subtract(m[0][1].multiply(m[1][0]))
    
        determinant = 0
        for c in range(len(m)):
            determinant += ((-1)**c)*m[0][c]*getMatrixDeternminant(getMatrixMinor(m,0,c))
        return determinant
    
    def getMatrixInverse(m):
        determinant = getMatrixDeternminant(m)
        #special case for 2x2 matrix:
        if len(m) == 2:
            return [[m[1][1]/determinant, -1*m[0][1]/determinant],
                    [-1*m[1][0]/determinant, m[0][0]/determinant]]
    
        #find matrix of cofactors
        cofactors = []
        for r in range(len(m)):
            cofactorRow = []
            for c in range(len(m)):
                minor = getMatrixMinor(m,r,c)
                cofactorRow.append(((-1)**(r+c)) * getMatrixDeternminant(minor))
            cofactors.append(cofactorRow)
        cofactors = transposeMatrix(cofactors)
        for r in range(len(cofactors)):
            for c in range(len(cofactors)):
                cofactors[r][c] = cofactors[r][c]/determinant
        return cofactors

    def gen_transition_matrix(matrix):
        for row in matrix:
            total = 0
            for val in row:
                total += val

            for val in row:
                val = Fraction(val, total)
