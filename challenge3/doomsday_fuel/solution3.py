from fractions import Fraction

def answer(m):
    transition_probability_matrix = m
    for i in range(len(m)):
        total = 0
        for j in range(len(m[0])):
            total += m[i][j] 

        if total == 0:
            m[i][i] = Fraction(1)
        else:
            for j in range(len(m[0])):
                # m[i][j] = m[i][j] / total
                m[i][j] = Fraction(m[i][j], total)

    print(transition_probability_matrix)

answer([[0, 1, 0, 0, 0, 1], [4, 0, 0, 3, 2, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]])
