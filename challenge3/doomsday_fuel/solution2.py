def answer(m):
    class Fraction:
        def __init__(self, numerator, denominator):
            self.numerator = numerator
            self.denominator = denominator

        def multiply(self, otherFraction):
            return Fraction(self.numerator * otherFraction.numerator, self.denominator * otherFraction.denominator)
            
        def add(self, otherFraction):
            def lcm(x, y):
                if x > y:
                    z = x
                else:
                    z = y

                while(True):
                    if((z % x == 0) and (z % y == 0)):
                        lcm = z
                        break
                    z += 1

                return lcm

            common_lcm = lcm(self.denominator, otherFraction.denominator)

            frac1_multipler = common_lcm / self.denominator
            frac2_multiplier = common_lcm / otherFraction.denominator

            num1Multiplied = self.numerator * frac1_multipler
            num2Multiplied = otherFraction.numerator * frac2_multiplier

            return Fraction(num1Multiplied + num2Multiplied, common_lcm)

        def subtract(self, toSubtract):
             toSubFlipped = Fraction(-toSubtract.numerator, toSubtract.denominator)

             return self.add(toSubFlipped)
   
        def reduce(self):
            def gcd(a, b):
                while b:
                    a, b = b, a % b

            divisor = gcd(self.denominator, self.numerator)

            self.numerator = self.numerator / divisor
            self.denominator = self.denominator / divisor

    # Boolean array
    # 0: If the current state is terminal
    # 1: If the current state is referenced
    term_ref = []

    for state in m:
        term_ref.append([True, False])

    for state_num, state in enumerate(m):
        for ref_num, reference in enumerate(state):
            if reference != 0:
                term_ref[state_num][0] = False
                term_ref[ref_num][1] = True

    # Boolean array
    # How many times the hits were accessed
    hit_tracebacks = []
    
    for state in term_ref:
        if state[0] and state[1]:
            hit_tracebacks.append([2, []])
        else:
            hit_tracebacks.append([2, []])

    def generate_traceback(queue):
        state = queue[0][0]
        trace = queue[0][1]

        newTrace = trace + [state]
        newQueue = queue[1:]

        if term_ref[state][0]:
            hit_tracebacks[state][1].append(newTrace)
        
        else:
            for transition in range(len(m[state])):
                if m[state][transition] != 0 and hit_tracebacks[transition][0] > 0:
                    hit_tracebacks[transition][0] -= 1
                    newQueue.append([transition, newTrace])
        
        if newQueue:
            generate_traceback(newQueue)

    generate_traceback([[0, []]])

    transition_probability_matrix = m
    for i in range(len(m)):
        total = 0
        for j in range(len(m[0])):
            total += m[i][j] 

        if total == 0:
            m[i][i] = Fraction(1, 1)
        else:
            for j in range(len(m[0])):
                # m[i][j] = m[i][j] / total
                m[i][j] = Fraction(m[i][j], total)

    for state in range(len(term_ref)):
        if term_ref[state][0]:
            if not term_ref[state][1]:
                print("State " + str(state))
                print("Probality = 0")

            else:
                base_case = hit_tracebacks[state][1][0]
                
                # bc_probablity = 1
                bc_probablity = Fraction(1, 1)
                for i in range(len(base_case) - 1):
                   current_state = base_case[i] 
                   next_state = base_case[i + 1]

                   # bc_probablity *= transition_probability_matrix[current_state][next_state]
                   bc_probablity = bc_probablity.multiply(transition_probability_matrix[current_state][next_state]) 
                
                geometric_step = hit_tracebacks[state][1][1]
                pattern_end = len(geometric_step) - 2
                pattern_start = -1

                for i in reversed(range(pattern_end)):
                    if geometric_step[i] == geometric_step[pattern_end]:
                        pattern_start = i
                        break

                repeated = geometric_step[pattern_start : pattern_end + 1]
                # geometric_probability = 1
                geometric_probability = Fraction(1, 1)
                for i in range(len(repeated) - 1):
                    current_state = repeated[i]
                    next_state = repeated[i + 1]
                    # geometric_probability *= transition_probability_matrix[current_state][next_state]
                    geometric_probability = geometric_probability.multiply(transition_probability_matrix[current_state][next_state])

                print("State " + str(state))
                print("Geometric Probabilyt") 
                print(geometric_probability)
                # probability = bc_probablity * (1 / (1 - geometric_probability))
                probability = bc_probablity.multiply(Fraction(geometric_probability.denominator, 
                    geometric_probability.denominator - geometric_probability.numerator))
                print("final probabilyt")
                print(probability.numerator)
                print(probability.denominator)

    print(term_ref)
    print(hit_tracebacks)
    print(transition_probability_matrix)

answer([[0, 1, 0, 0, 0, 1], [4, 0, 0, 3, 2, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]])
answer([[0, 2, 1, 0, 0], [0, 0, 0, 3, 4], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]])
