def answer(M, F):
    class Node:
        def __init__(self, m, f, parent=None):
            self.m = m
            self.f = f
            self.parent = parent
        
        def generate_children(self):
            child1 = Node(self.m, self.f + self.m, self)
            child2 = Node(self.m + self.f, self.f, self)
            
            return [child1, child2]
        
        def equals(self, other_node):
            return self.m == other_node.m and self.f == other_node.f
    
    def BFS(starting_m, starting_f, ending_m, ending_f):
        starting_node = Node(starting_m, starting_f)

        visited_nodes = set()
        queue = [starting_node]
        
        while queue:
            current_node = queue.pop(0)
            
            print("Queue length: " + str(len(queue)))
            print("Visted Nodes: " + str(len(visited_nodes)))
            print("M: " + str(current_node.m))
            print("F: " + str(current_node.f))
            print("\n")
            
            node_id = "M" + str(current_node.m) + "F" + str(current_node.f)
             
            if node_id not in visited_nodes and current_node.m <= ending_m and current_node.f <= ending_f:
                visited_nodes.add(node_id)
                
                if current_node.m == ending_m and current_node.f == ending_f:
                    return str(reconstruct_path(current_node))
                
                queue += current_node.generate_children()
        
        return "impossible"
    
    def reconstruct_path(node):
        generations = 0
        
        while node.parent != None:
            node = node.parent
            generations += 1

        return generations

    return BFS(1, 1, int(M), int(F))

print(answer('4', '7'))
print(answer('2', '1'))
print(answer('2', '4'))
print(answer('200', '400'))
