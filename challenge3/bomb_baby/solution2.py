def answer(M, F):
    M = int(M)
    F = int(F)

    if M % 2 == 0 and F % 2 == 0:
        return "impossible"
    
    generations = 0
    
    currentM = M
    currentF = F
    
    while True:
        print("Current M: " + str(currentM))
        print("Current F: " + str(currentF))
        print("\n")

        if currentM == 1 and currentF == 1:
            return str(generations)

        if currentM - currentF > 0:
            currentM -= currentF
            generations += 1
        
        elif currentF - currentM > 0:
            currentF -= currentM
            generations += 1
        
        else:
            return "impossible"

print(answer('4', '7'))
print(answer('2', '1'))
print(answer('25723', '15843'))
print(answer('2', '4'))
print(answer('200', '400'))
#print(answer('5658654546545645645645645103864565645456456645646546546546544448785456135486434864654655', '5658654546545645645645645645645646545645645645646546546546544448785456135486434864654655'))
print(answer('10005', '154845281021548100215480485215481545542105'))
